#asm
global _LCD_OUT
global _MYASM
SIGNAT _MYASM,8192
  
PSECT mytext,local,class=CODE,delta=2

_MYASM:
    ;If this returns it will be run again.
    
    MOVLW 'a'
    call OUTCHAR
    
    return
  
WAIT_FOR_WRITE:
    ;Wait for transmit to complete
S1: TSTFSZ _LCD_OUT
    bra S1
    
    return
    
OUTCHAR:
    ;;Assume that 
    MOVWF _LCD_OUT
    
    ;;Wait for write to LCD to complete
    call WAIT_FOR_WRITE
    return
#endasm